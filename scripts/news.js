function writeNews() {
    function write_datas(articles) {
        var content = "";
        articles = articles.split("\n\n\n\n");

        for (article of articles) {
            var articleHTML = "";
            for (line of article.split("\n")) {
                if (line.startsWith("!")) {
                    if (line.startsWith("![title]")) {
                        articleHTML += "<h2>" + line.replace("![title]", "") + "</h2><hr />";
                    } else if (line.startsWith("![author]")) {
                        articleHTML += "<h4>" + line.replace("![author]", "") + "</h4>";
                    } else if (line.startsWith("![sub]")) {
                        articleHTML += "<h3>" + line.replace("![sub]", "") + "</h3>";
                    } else if (line.startsWith("![image]")) {
                        articleHTML += "" + "<img alt='" + line.replace("![image]","") + "' src=\"./medias/news/" + line.replace("![image]", "") + "\" />"
                    } else if (line.startsWith("![video]")) {
                        articleHTML += "" + "<video src=\"./medias/news/" + line.replace("![video]", "") + "\" controls=\"controls\"></video>"
                    }
                } else {
                    articleHTML += "<p>" + line + "</p>";
                }
            }
            content += "<div class='box'>" + articleHTML + "</div>";
        }
        document.getElementById("news").innerHTML = content;
    }

    var request;
    request = new XMLHttpRequest();
    var datas;

    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            write_datas(request.responseText);
            console.log(request.responseText)
            // write_datas(request.responseText);
        }
    };
    request.open("GET", "./texts/news.txt", true);
    request.send();
}

