var images = [
    "./medias/banners/gamers.jpg",
    "./medias/banners/newgame.jpg",
    "./medias/banners/planetarian.jpg"
];

var numOfImages = images.length;
var index = 0;

function slidingImages() {
    document.getElementById("bannerImg").src = images[index];
    index += 1;
    if (index === numOfImages)
        index = 0;

    setTimeout(slidingImages, 2000);
}
