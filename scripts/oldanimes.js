function writeOldAnimes() {
    function write_datas(articles) {
        var content = "";
        articles = articles.split("\n\n\n\n\n");
        let template = "        <div class=\"box\">\n" +
            "            <div style=\"float:left; width: 30%\">\n" +
            "                <img alt='{1}'  style=\"height: auto;\" src=\"{0}\" />\n" +
            "            </div>\n" +
            "            <h1>{1}</h1><hr />\n"+
            "            <p style=\"float: left; width: 65%\">{2}</p>\n" +
            "            <div style=\"clear: both\"> </div>\n" +
            "        </div>";
        for (article of articles) {
            var lines = article.split("\n");
            articleHTML = template.format(lines[0], lines[1], lines.slice(2).join("<br />"));
            console.log(lines.slice(2, -1).join("<br />"));
            content += articleHTML;
        }
        document.getElementById("oldAnime").innerHTML = content;
    }

    var request;
    request = new XMLHttpRequest();
    var datas;

    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            write_datas(request.responseText);
        }
    };
    request.open("GET", "./texts/oldanimes.txt", true);
    request.send();
}

