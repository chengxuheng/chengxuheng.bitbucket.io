function setForm() {
    document.forms[0].onsubmit = function () {
        var form = document.forms[0];
        var timeStamp = new Date().getTime().toString();
        data = new Object();
        for (item of form) {
            data[item.name] = item.value;
        }
        var forms;
        if (localStorage.hasOwnProperty("feedbacks")) {
            forms = JSON.parse(localStorage.getItem('feedbacks'));
        } else {
            forms = new Object();
        }
        forms[timeStamp] = data;
        // console.log(forms);
        localStorage.setItem('feedbacks', JSON.stringify(forms));
    }
}

function setClear() {
    document.getElementById("clear").onclick = function () {
        if (localStorage.hasOwnProperty("feedbacks")) {
            localStorage.setItem("feedbacks", "{}");
        }
        location.reload();
    }
}


function writeFeedbacks() {
    feedbackTemplete =
        "    <div class=\"box\">\n" +
        "        <h3>Author: {0}</h3>\n" +
        "        <h4>Date: {1}</h4>\n" +
        "        <h4>Age: {2}</h4>\n" +
        "        <h4>E-mail: <a href='mailto:{3}'>{3}</a></h4>" +
        "        <hr/>\n" +
        "        <p>{4}</p>\n" +
        "    </div>";

    if (localStorage.hasOwnProperty("feedbacks")) {
        var feedbacks = JSON.parse(localStorage.feedbacks);
        // console.log(feedbacks);
        for (var timeStamp in feedbacks) {
            var data = feedbacks[timeStamp];
            var time = new Date(parseInt(timeStamp));
            console.log(timeStamp, data);
            var date = "{0}-{1}-{2} {3}:{4}:{5}".format(
                time.getFullYear(),
                ("0" + time.getMonth()).slice(-2),
                ("0" + time.getDate()).slice(-2),
                ("0" + time.getHours()).slice(-2),
                ("0" + time.getMinutes()).slice(-2),
                ("0" + time.getSeconds()).slice(-2)
            );
            var element = document.getElementById("feedbacks");
            element.innerHTML = feedbackTemplete.format(data['name'], date,
                data['age'], data['mail'],
                data['comment'].replace(/\n/g, "</p><p>")) + element.innerHTML;
        }
    }
}
