function highLightNavbar() {
    document.getElementById("navbar").childNodes.forEach(function (node) {
        if (node.firstChild !== null) {
            if (document.title.indexOf(node.firstChild.textContent) !== -1) {
                node.firstChild.style.backgroundColor = 'rgb(106, 226, 204)';
            }
            console.log(node.firstChild.textContent);
        }
    });
}

String.prototype.format= function(){
    let args = arguments;
    return this.replace(/\{(\d+)\}/g, function(s,i){
        return args[i];
    });
};
