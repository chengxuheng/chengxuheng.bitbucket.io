# Final Report
#### Xuheng CHENG 15398078

### Important:
Some of my pages used AJAX on localhost, 
they don't work when double-click on disk. 
And it cannot be validated. So I made rendered pages
of them to check accessibility.(<code>*.rendered.html</code>).


## Introduction
### Intended Audience:
People who like watching anime(Japanese animations).
Most of them are in age of 15-25, about 70% male.

### Purpose of the Site:
Share some news about anime with enthusiasts of anime.

### Opportunity, Problem and Issue:
I want to improve my JavaScript skill so I will
render some page with JavaScript and AJAX.  
I think there would be no problem because I am very
familiar with anime.

### Related sites:
Kotaku, this site has some anime news that I am
fond of.
https://kotaku.com/tag/anime  

My Anime List, this site has anime list of each season.
https://myanimelist.net/anime/season


## Analysis

#### Web Site Goal
I'd like to share some news about animation
with other fans of animation.

#### What information do I need?
News of anime, and schedule of new updating animes

#### How will your project use a form?
Offering feedback. I made a form with four items:
name, mail, age and feedback.
It can give me the most important information
that I need to improve my site.

### Site Map
![sitemap](sitemap.png)

#### Wireframe
![wireframe](wireframe.png)

## A critique of the site:
#### What do you think is good?
I do not want to write HTML tags by my hand,
that's so boring.

I wrote many scripts to render my page instead
directly write HTML. Although JS and AJAX
are not criteria to get mark, however, I
think it simplified my coding. 
It need more thinking, but it removed
all boring works. I like it.

#### Given more time, what would you improve?
I will write more CSS rules and add better images to
have a better view.

## Accessibility
##### Perceivable
I added alt attributes to all images, my site has
no content rely on colour alone. What's more, 
I used stylesheet to give my site a better view. 
Besides, I created a few tables to show information
in a page. 

##### Operable
Users have enough time to read and use content because
it would be updated unless they click another href.
I didn't add any content which might cause seizures.

##### Understandable
My English follows grammer. And anything would be shown
in a predictable way. My form have a pattern attribute so
users would not type wrong data.

##### Robust
I have tested my site, it works on Chrome, Firefox,
Safari and Opera.


## Appendix:
#### Include all the JavaScript code and the related HTML.

This file, <code>utilfunctions.js</code>, is two functions that I will need to use in multiple pages.
The first function is to highlight the page which is opened on the
navigator, all pages used this function.
The second function is to format my string in other scripts.  

    function highLightNavbar() {
        document.getElementById("navbar").childNodes.forEach(function (node) {
            if (node.firstChild !== null) {
                if (document.title.indexOf(node.firstChild.textContent) !== -1) {
                    node.firstChild.style.backgroundColor = 'rgb(106, 226, 204)';
                }
                console.log(node.firstChild.textContent);
            }
        });
    }
    
    String.prototype.format= function(){
        let args = arguments;
        return this.replace(/\{(\d+)\}/g, function(s,i){
            return args[i];
        });
    };

The second most important script is the <code>feedback.js</code>, which has
functions to save feedback form on localStorage
and print them and clear them.

    function setForm() {
        document.forms[0].onsubmit = function () {
            var form = document.forms[0];
            var timeStamp = new Date().getTime().toString();
            data = new Object();
            for (item of form) {
                data[item.name] = item.value;
            }
            var forms;
            if (localStorage.hasOwnProperty("feedbacks")) {
                forms = JSON.parse(localStorage.getItem('feedbacks'));
            } else {
                forms = new Object();
            }
            forms[timeStamp] = data;
            // console.log(forms);
            localStorage.setItem('feedbacks', JSON.stringify(forms));
        }
    }
    
    function setClear() {
        document.getElementById("clear").onclick = function () {
            if (localStorage.hasOwnProperty("feedbacks")) {
                localStorage.setItem("feedbacks", "{}");
            }
            location.reload();
        }
    }
    
    
    function writeFeedbacks() {
        feedbackTemplete =
            "    <div class=\"box\">\n" +
            "        <h3>Author: {0}</h3>\n" +
            "        <h4>Date: {1}</h4>\n" +
            "        <h4>Age: {2}</h4>\n" +
            "        <h4>E-mail: <a href='mailto:{3}'>{3}</a></h4>" +
            "        <hr/>\n" +
            "        <p>{4}</p>\n" +
            "    </div>";
    
        if (localStorage.hasOwnProperty("feedbacks")) {
            var feedbacks = JSON.parse(localStorage.feedbacks);
            // console.log(feedbacks);
            for (var timeStamp in feedbacks) {
                var data = feedbacks[timeStamp];
                var time = new Date(parseInt(timeStamp));
                console.log(timeStamp, data);
                var date = "{0}-{1}-{2} {3}:{4}:{5}".format(
                    time.getFullYear(),
                    ("0" + time.getMonth()).slice(-2),
                    ("0" + time.getDate()).slice(-2),
                    ("0" + time.getHours()).slice(-2),
                    ("0" + time.getMinutes()).slice(-2),
                    ("0" + time.getSeconds()).slice(-2)
                );
                var element = document.getElementById("feedbacks");
                element.innerHTML = feedbackTemplete.format(data['name'], date,
                    data['age'], data['mail'], 
                    data['comment'].replace(/\n/g, "</p><p>")) + element.innerHTML;
            }
        }
    }

The third most important js is the <code>banner.js</code>. Which
change the banner image in the home page.

    var images = [
        "./medias/banners/gamers.jpg",
        "./medias/banners/newgame.jpg",
        "./medias/banners/planetarian.jpg"
    ];
    
    var numOfImages = images.length;
    var index = 0;
    
    function slidingImages() {
        document.getElementById("bannerImg").src = images[index];
        index += 1;
        if (index === numOfImages)
            index = 0;
    
        setTimeout(slidingImages, 2000);
    }


Other scripts:
<code>news.js</code> is to render <code>animenews.html</code>

    function writeNews() {
        function write_datas(articles) {
            var content = "";
            articles = articles.split("\n\n\n\n");
    
            for (article of articles) {
                var articleHTML = "";
                for (line of article.split("\n")) {
                    if (line.startsWith("!")) {
                        if (line.startsWith("![title]")) {
                            articleHTML += "<h2>" + line.replace("![title]", "") + "</h2><hr />";
                        } else if (line.startsWith("![author]")) {
                            articleHTML += "<h4>" + line.replace("![author]", "") + "</h4>";
                        } else if (line.startsWith("![sub]")) {
                            articleHTML += "<h3>" + line.replace("![sub]", "") + "</h3>";
                        } else if (line.startsWith("![image]")) {
                            articleHTML += "" + "<img alt='" + line.replace("![image]","") + "' src=\"./medias/news/" + line.replace("![image]", "") + "\" />"
                        } else if (line.startsWith("![video]")) {
                            articleHTML += "" + "<video src=\"./medias/news/" + line.replace("![video]", "") + "\" controls=\"controls\"></video>"
                        }
                    } else {
                        articleHTML += "<p>" + line + "</p>";
                    }
                }
                content += "<div class='box'>" + articleHTML + "</div>";
            }
            document.getElementById("news").innerHTML = content;
        }
    
        var request;
        request = new XMLHttpRequest();
        var datas;
    
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {
                write_datas(request.responseText);
                console.log(request.responseText)
                // write_datas(request.responseText);
            }
        };
        request.open("GET", "./texts/news.txt", true);
        request.send();
    }



<code>newanimes.js</code> is to render <code>newanimes.html</code>

    function writeNewAnimes() {
        function write_datas(articles) {
            var content = "";
            articles = articles.split("\n\n\n\n\n");
            let template = "        <div class=\"box\">\n" +
                "            <div style=\"float:left; width: 30%\">\n" +
                "                <img alt='{1}' style=\"height: auto;\" src=\"{0}\" />\n" +
                "            </div>\n" +
                "            <h1>{1}</h1><hr/>\n"+
                "            <p style=\"float: left; width: 65%\">{2}</p>\n" +
                "            <div style=\"clear: both\"> </div>\n" +
                "        </div>";
            for (article of articles) {
                var lines = article.split("\n");
                articleHTML = template.format(lines[0], lines[1], lines.slice(2).join("<br />"));
                console.log(lines.slice(2, -1).join("<br />"));
                content += articleHTML;
            }
            document.getElementById("newAnime").innerHTML = content;
        }
    
        var request;
        request = new XMLHttpRequest();
        var datas;
    
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {
                write_datas(request.responseText);
            }
        };
        request.open("GET", "./texts/newanimes.txt", true);
        request.send();
    }

<code>oldanimes.js</code> is to render <code>oldanimes.html</code>

    function writeOldAnimes() {
        function write_datas(articles) {
            var content = "";
            articles = articles.split("\n\n\n\n\n");
            let template = "        <div class=\"box\">\n" +
                "            <div style=\"float:left; width: 30%\">\n" +
                "                <img alt='{1}'  style=\"height: auto;\" src=\"{0}\" />\n" +
                "            </div>\n" +
                "            <h1>{1}</h1><hr />\n"+
                "            <p style=\"float: left; width: 65%\">{2}</p>\n" +
                "            <div style=\"clear: both\"> </div>\n" +
                "        </div>";
            for (article of articles) {
                var lines = article.split("\n");
                articleHTML = template.format(lines[0], lines[1], lines.slice(2).join("<br />"));
                console.log(lines.slice(2, -1).join("<br />"));
                content += articleHTML;
            }
            document.getElementById("oldAnime").innerHTML = content;
        }
    
        var request;
        request = new XMLHttpRequest();
        var datas;
    
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {
                write_datas(request.responseText);
            }
        };
        request.open("GET", "./texts/oldanimes.txt", true);
        request.send();
    }

